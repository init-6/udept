udept
=====

[udept is a collection of Portage scripts, maintenance tools and analysis tools, written in bash and powered by the dep engine.](https://github.com/init6/udept/wiki)